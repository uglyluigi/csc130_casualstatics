def get_integer():
	return input("Enter an integer: ")

#Gets the minimum value in a list of integers
def minimum(int_list):
	min = int_list[0]
	index = 0
	while index < len(int_list) - 1: #Iterate over the list until the index reaches len(list) - 1 since lists are 0-based
		if int_list[index] < min:
			min = int_list[index] #Set the newly found minimum
		index += 1
	return min

def maximum(int_list):
	max = int_list[0]
	index = 0
	while index < len(int_list):
		if int_list[index] > max:
			max = int_list[index]
		index += 1
	return max

def mean(int_list):
	sum = 0
	for i in int_list:
		sum += i
	return sum / len(int_list)

#Returns the median value of the list.
def median(int_list):
	index = 0
	list_minimum = minimum(int_list)
	list_maximum = maximum(int_list)
	while index < len(int_list) - 1:
		if int_list[index] > list_minimum and int_list[index] < list_maximum:
			return int_list[index]

get_integer()